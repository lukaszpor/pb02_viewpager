package pl.pawelzadykowicz.pb02_viewpager;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.View;

/**
 * Prosta wersja adaptera utrzymująca w pamieci wszystkie strony. Wystarczy
 * dziedziczyc z {@link FragmentStatePagerAdapter} aby mieć efekt usuwania z
 * pamięci fragmentów - zmniejszenie zuzycia pamięci, ale potencjalnie występic
 * moga opóźnienia podczas ładowania widoku.
 * 
 * @author zadykowiczp
 * 
 */
public class MyPagerAdapter extends FragmentPagerAdapter {

	private static final int FRAGMENT_COUNT = 30;
	private Context kontekst;

	public MyPagerAdapter(FragmentManager fm, Context kontekst) {
		super(fm);
		this.kontekst = kontekst;
	}

	@Override
	public Fragment getItem(int position) {
		String fragmentText = kontekst.getString(R.string.fragment_no) + position;
		return PageFragment.newInstance(fragmentText);
	}

	@Override
	public int getCount() {
		return FRAGMENT_COUNT;
	}
	
	@Override
	public CharSequence getPageTitle(int position) {
		return kontekst.getString(R.string.title) + position;
	}

}
