package pl.pawelzadykowicz.pb02_viewpager;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.View;

/**
 * musimy dziedziczyć z {@link FragmentActivity} ponieważ
 * {@link FragmentPagerAdapter} wymaga {@link FragmentManager} z wersji z
 * support library. Tak samo {@link ViewPager} wymaga fragmentów z support
 * library.
 * 
 * @author zadykowiczp
 * 
 */
public class MainActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
		PagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager(), getApplicationContext());
		viewPager.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
