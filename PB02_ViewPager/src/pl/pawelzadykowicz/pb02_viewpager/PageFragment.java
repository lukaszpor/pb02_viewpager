package pl.pawelzadykowicz.pb02_viewpager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PageFragment extends Fragment {

	private static final String FRAGMENT_TEXT = "fragment_text";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.page_fragment, null);
		TextView tvLabel = (TextView) view.findViewById(R.id.tvLabel);
		CharSequence text = getArguments().getCharSequence(FRAGMENT_TEXT);
		tvLabel.setText(text );
		return view;
	}

	public static Fragment newInstance(String fragmentText) {
		Fragment frag = new PageFragment();
		Bundle args = new Bundle();
		args.putCharSequence(FRAGMENT_TEXT, fragmentText);
		frag.setArguments(args );
		return frag ;
	}
}
